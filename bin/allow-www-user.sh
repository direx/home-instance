#!/bin/bash

USER="${1:- direx}"
WWWPATH="${2:- /var/www}"



#sudo find ${WWWPATH} -type f -exec sudo chmod 664 {} \;
sudo adduser ${USER} www-data
sudo usermod -a -G www-data ${USER}
sudo chown -R www-data:www-data ${WWWPATH}
sudo chmod -R g+rwX ${WWWPATH}

sudo find /var/www -type f -exec sudo chmod 644 {} \;
sudo find /var/www -type d -exec sudo chmod 755 {} \;
